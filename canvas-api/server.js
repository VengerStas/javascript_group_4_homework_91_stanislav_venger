const express = require('express');
const expressWs = require('express-ws');
const cors = require('cors');
const nanoid = require('nanoid');

const app = express();

const port = 8000;

expressWs(app);
app.use(cors());

const activeConnections = {};


app.ws('/square', (ws, req) => {
    const id = nanoid();
    console.log('client connected id=', id);
    activeConnections[id] = ws;

    ws.on('message', msg => {
        let createSquare;

        try{
            createSquare = JSON.parse(msg);
        } catch(e) {
            return console.log('Not a valid message')
        }

        switch(createSquare.type) {
            case 'CREATE_SQUARE':
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    conn.send(JSON.stringify({
                        type: 'NEW_SQUARE',
                        square: createSquare.square
                    }))
                });
                break;
            default:
                console.log('Not valid message type, ', createSquare.type);
        }
    });

    ws.on('close', msg => {
        console.log('client disconnected id=', id);
        delete activeConnections[id];
    });
});

app.listen(port, () => {
    console.log('server start on ' + port)
});