import React, {Component, createRef} from 'react';
import './App.css';

class App extends Component {
  state = {
    square: []
  };

  componentDidMount() {
    this.websocket = new WebSocket('ws://localhost:8000/square');

    this.websocket.onmessage = event => {
      const createSquare = JSON.parse(event.data);
      if (createSquare.type === 'NEW_SQUARE') {
        this.setState({
          square: [
              ...this.state.square,
              createSquare.square
          ]
        })
      }
    }
  }

  canvas = createRef();

  drawSquare = event => {
    event.preventDefault();

    const x = event.nativeEvent.offsetX;
    const y = event.nativeEvent.offsetY;

    this.websocket.send(JSON.stringify({
      type: 'CREATE_SQUARE',
      square: {
        x: x,
        y: y,
        width: Math.random() * 60,
        height: Math.random() * 60
      }
    }));
  };

  render () {
    this.state.square.forEach(square => {
      const ctx = this.canvas.current.getContext('2d');
      ctx.beginPath();
      ctx.fillRect(square.x, square.y, square.width, square.height );
      ctx.fillStyle = 'darkorange';
      ctx.stroke();
    });

    return (
        <div style={{padding: '50px'}}>
          <canvas ref={this.canvas} width="1200px" height="500px" style={{border: '1px solid black'}} onClick={this.drawSquare}/>
        </div>
    );
  }
}

export default App;
